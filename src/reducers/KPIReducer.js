import { ADD_KPI, REQUEST_KPIS, RECEIVE_KPIS, ADD_DETAILS } from '../actions/KPIActions'

const initialState = {
  kpis: [  ],
  details: {},
  isFetching: false,
  didInvalidate: false
}

export default function KPIReducer(state = initialState, action) {
  switch(action.type) {
    case ADD_KPI:
      return {
        ...state,
        kpis: [...state.kpis, { name: action.name, devId: action.devId } ]
      }
    case REQUEST_KPIS:
       return {
         ...state,
            isFetching: true,
            didInvalidate: false
          }
    case RECEIVE_KPIS:
       return {
         ...state,
            isFetching: false,
            didInvalidate: false
          }
    case ADD_DETAILS:
       return {
         ...state,
            details: {
                ...state.details,
                [action.devId]: {
                   ...state.details[action.devId],
                   [action.pos]: action.data}}
          }
    default:
      return state
  }
}
