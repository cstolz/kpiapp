import KPIReducer from './KPIReducer'
import { combineReducers } from 'redux'
import { persistCombineReducers } from 'redux-persist'
import { AsyncStorage } from 'react-native'

const config = {
  key: 'root',
  storage: AsyncStorage
}

const state = {
  KPIReducer: KPIReducer,
}

export default persistCombineReducers(config, state)
