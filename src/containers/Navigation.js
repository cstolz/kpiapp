import React from 'react'
import { Image, Button } from 'react-native'
import { StackNavigator } from 'react-navigation';

import KPIList from '../components/KPIList'
import DetailsScreen from '../components/DetailsScreen'

class LogoTitle extends React.Component {
  render() {
    return (
      <Image
        source={require('../../assets/kpi.png')}
        style={{ width: 30, height: 30 }}
      />
    );
  }
}

const RootStack = StackNavigator(
  {
    Home: {
      screen: KPIList,
    },
    Details: {
      screen: DetailsScreen,
    },
  },
  {
    initialRouteName: 'Home',
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#eeeeee',
      },
 //     headerTitle: <LogoTitle />,
      headerRight: (
        <Image
                source={require('../../assets/cog.png')}
                style={{ width: 30, height: 30 }}
              />
      ),
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  }
);

export default RootStack