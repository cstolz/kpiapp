import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Navigation from '../containers/Navigation'

import { fetchKPIsIfNeeded } from '../actions/KPIActions'

class AsyncApp extends Component {

  constructor(props) {
    super(props)
//    this.handleChange = this.handleChange.bind(this)
//    this.handleRefreshClick = this.handleRefreshClick.bind(this)
  }

  componentDidMount() {
    const { dispatch } = this.props
    dispatch(fetchKPIsIfNeeded())
  }

  render() {

    return(
      <Navigation />
    )
  }
}

AsyncApp.propTypes = {
  kpis: PropTypes.array.isRequired,
  isFetching: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  const { KPIReducer } = state
  const {
    isFetching,
    kpis: kpis
  } = KPIReducer || {
    isFetching: true,
    kpis: []
  }

  return {
    kpis,
    isFetching
  }
}

export default connect(mapStateToProps)(AsyncApp)
