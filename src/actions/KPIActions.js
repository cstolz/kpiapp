export const ADD_KPI = 'ADD_KPI'
export const REQUEST_KPIS = 'REQUEST_KPIS'
export const REQUEST_DETAILS = 'REQUEST_DETAILS'
export const RECEIVE_KPIS = 'RECEIVE_KPIS'
export const RECEIVE_DETAILS = 'RECEIVE_DETAILS'
export const ADD_DETAILS = 'ADD_DETAILS'

export function addKPI(id, name) {
  return {
    type: ADD_KPI,
    name: name,
    devId: id
  }
}

export function addDetail(devId, pos, data) {
  return {
    type: ADD_DETAILS,
    devId: devId,
    data: data,
    pos: pos
  }
}

function requestKPIs() {
  return {
    type: REQUEST_KPIS
  }
}

export function requestDetails(devId) {
  return dispatch => {
    for(let i = 0; i < 24; i++) {
        console.log(`https://gvp.cstolz.de/rest/1/projects/JanHome/devices/${devId}/hist/energy/UserDefined/Overall.json?start=NAMED_LastMonth&end=NAMED_LastMonth&anchor=RELATIVE_-${i}MONTH`)
        fetch(`https://gvp.cstolz.de/rest/1/projects/JanHome/devices/${devId}/hist/energy/UserDefined/Overall.json?start=NAMED_LastMonth&end=NAMED_LastMonth&anchor=RELATIVE_-${i}MONTH`)
          .then(response => response.json())
          .then(json => {
            receiveDetails(dispatch, json, devId, i)
          })
          .catch((error) => { receiveDetails(dispatch, {energy: 0.0 }, devId, i); })
      }
  }
}

function receiveDetails(dispatch, json, devId, pos) {
  dispatch(addDetail(devId, pos, json ))
}

function receiveKPIs(dispatch, json) {
  dispatch({ type: RECEIVE_KPIS })
  for(var device of json.device) {
    if(device.type == 'XXXVirtualKPI') {
      dispatch(addKPI(device.id, device.name))
    }
  }
}

function fetchKPIs() {
  return dispatch => {
    dispatch(requestKPIs())
    return fetch(`https://gvp.cstolz.de/rest/1/projects/JanHome/devices.json`)
      .then(response => response.json())
      .then(json => {
        console.log(json)
        receiveKPIs(dispatch, json)
      })
      .catch((error) => { console.error(error); })
  }
}
function shouldFetchKPIs(state) {
  const kpis = state.KPIReducer.kpis
  if (!kpis.length > 0) {
    return true
  } else if (state.KPIReducer.isFetching) {
    return false
  } else {
    return state.KPIReducer.didInvalidate
  }
}

export function fetchKPIsIfNeeded() {
  return (dispatch, getState) => {
    if (shouldFetchKPIs(getState())) {
      return dispatch(fetchKPIs())
    }
  }
}
