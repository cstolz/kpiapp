import React from 'react'
import { View, Text, StyleSheet, Image, FlatList, ListItem, TouchableOpacity, ActivityIndicator, Button } from 'react-native'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

class KPIList extends React.Component {

  static navigationOptions = {
    title: 'Details'
  };


  renderKPI = ({ item, index }) => {
    const { params } = this.props.navigation.state;
    const itemId = params ? params.itemId : null;
    const kpidata = this.props.kpival[itemId]
    var kpi
    if(typeof kpidata != "undefined") {
      kpi = kpidata[index]
    }
    if(typeof kpi === "undefined") {
    return (
    <TouchableOpacity style={ [styles.kpi_item_box, index %2 == 0 ? styles.kpi_item_box_even : styles.kpi_item_box_odd]}>
                <Image source={require('../../assets/kpi.png')} style={{width: 32, height: 32}} />
                <ActivityIndicator />
                </TouchableOpacity>
              )
    } else {
        return (
          <TouchableOpacity style={ [styles.kpi_item_box, index %2 == 0 ? styles.kpi_item_box_even : styles.kpi_item_box_odd]}>
            <Image source={require('../../assets/kpi.png')} style={{width: 32, height: 32}} />
            <Text>
              {itemId} {Math.round(kpi.energy*100)/100} {typeof kpi.valueType === "undefined" ? "--" : kpi.valueType.unit}
            </Text>
          </TouchableOpacity>
        )
    }
  }

  render() {
    var list = [];
    for (var i = 0; i < 24; i++) {
        list.push(i.toString());
    }
    return (
      <View style={styles.kpi_list}>
        <FlatList
          data={ list }
          renderItem={this.renderKPI}
         />
      </View>
    )
  }
}

KPIList.propTypes = {
  kpis: PropTypes.array.isRequired,
  selected: PropTypes.string.isRequired,
//  kpival: PropTypes.map.isRequired,
  isLoading: PropTypes.bool.isRequired,
}

const mapStateToProps = (state, nextProps) => {
  return {
    kpis: state.KPIReducer.kpis,
    kpival: state.KPIReducer.details,
    selected: "6",
    isLoading: state.KPIReducer.isFetching
  }
}
const styles = StyleSheet.create({
  kpi_list: {
   flex: 1,
   paddingTop: 22,
   paddingLeft: 10,
   paddingRight: 10
  },
  kpi_item_box: {
   flex: 1,
   flexDirection: 'row',
   alignItems: 'center'
  },
  kpi_item_box_even: {
   backgroundColor: 'green'
  },
  kpi_item_box_odd: {
   backgroundColor: 'lightgray'
  },
  kpi_item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  kpi_item_selected: {
    fontWeight: 'bold'
  },
})

export default connect(mapStateToProps)(KPIList)
