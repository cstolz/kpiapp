import React from 'react'
import { View, Text, StyleSheet, Image, FlatList, ListItem, TouchableOpacity, ActivityIndicator, Button } from 'react-native'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { requestDetails } from '../actions/KPIActions'

class KPIList extends React.Component {

  static navigationOptions = {
    title: 'KPIs'
  };
y
  renderKPI = ({ item, index }) => {
    return (
      <TouchableOpacity
          style={ [styles.kpi_item_box, index %2 == 0 ? styles.kpi_item_box_even : styles.kpi_item_box_odd]}
          onPress={() => {
                      this.props.dispatch(requestDetails(item.devId))
                      this.props.navigation.navigate('Details', {
                        itemId: item.devId,
                      });
                    }}
      >
        <Image source={require('../../assets/kpi.png')} style={{width: 32, height: 32}} />
        <Text style={[styles.kpi_item, this.props.selected == item.devId.toString() ? styles.kpi_item_selected: styles.kpi_item]}>{item.name}</Text>
      </TouchableOpacity>
    )
  }

  render() {
    if(this.props.isLoading){
      return(
        <ActivityIndicator/>
      )
    }
    return (
      <View style={styles.kpi_list}>
        <FlatList
          keyExtractor={(kpi) => kpi.devId.toString()}
          data={this.props.kpis}
          renderItem={this.renderKPI}
          title="KPIs"
         />
      </View>
    )
  }
}

KPIList.propTypes = {
  kpis: PropTypes.array.isRequired,
  selected: PropTypes.string.isRequired,
  isLoading: PropTypes.bool.isRequired,
}

const mapStateToProps = (state, nextProps) => {
  return {
    kpis: state.KPIReducer.kpis,
    selected: "6",
    isLoading: state.KPIReducer.isFetching
  }
}
const styles = StyleSheet.create({
  kpi_list: {
   flex: 1,
   paddingTop: 22,
   paddingLeft: 10,
   paddingRight: 10
  },
  kpi_item_box: {
   flex: 1,
   flexDirection: 'row',
   alignItems: 'center'
  },
  kpi_item_box_even: {
   backgroundColor: 'gray'
  },
  kpi_item_box_odd: {
   backgroundColor: 'lightgray'
  },
  kpi_item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  kpi_item_selected: {
    fontWeight: 'bold'
  },
})

export default connect(mapStateToProps)(KPIList)
